from rest_framework.routers import SimpleRouter
from .views import OrderItemViewSet, WidgetViewSet

router = SimpleRouter()
router.register('orders', OrderItemViewSet, base_name='orders')
router.register('widgets', WidgetViewSet, base_name='widgets')

urls = router.urls