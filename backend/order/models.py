from datetime import timedelta
from django.utils.timezone import now
from django.db import models


class Gate(models.Model):
    number = models.SmallIntegerField(null=True, blank=True)
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return 'Ворота №{}'.format(self.number)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.end or (self.start > self.end):
            self.end = self.start + timedelta(hours=2)
        super(Gate, self).save(force_insert, force_update, using, update_fields)


class OrderItem(models.Model):
    pack_list = models.CharField(max_length=20, null=True, blank=True, unique=True)
    customer = models.CharField(max_length=20, null=True, blank=True)
    returned = models.BooleanField(default=False)
    late = models.BooleanField(default=False)
    start = models.DateTimeField(null=True, blank=True)
    gate = models.ForeignKey('Gate', on_delete=models.CASCADE, null=True, blank=True)
    widget = models.ForeignKey('Widget', on_delete=models.CASCADE)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.start:
            self.start = now()
        super(OrderItem, self).save(force_insert, force_update, using, update_fields)


class Widget(models.Model):
    name = models.CharField(max_length=15)

    def __str__(self):
        return self.name
