from rest_framework.serializers import ModelSerializer, SerializerMethodField
from .models import Gate, OrderItem, Widget


class GateFKSerializer(ModelSerializer):

    class Meta:
        model = Gate
        fields  = ('number', 'id', 'start')


class WidgetFKSerializer(ModelSerializer):

    class Meta:
        model = Widget
        fields = ('name', 'id')

class OrderItemSerializer(ModelSerializer):

    gate_number = SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = '__all__'

    def get_gate_number(self, obj):
        return obj.gate.number


class WidgetSerializer(ModelSerializer):

    class Meta:
        model = Widget
        fields = '__all__'