from django.contrib import admin
from .models import Gate, OrderItem, Widget


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'start', 'gate')


admin.site.register(Gate)
admin.site.register(OrderItem, OrderAdmin)
admin.site.register(Widget)