from django.db.models import Q
from datetime import timedelta
from dateutil.parser import parse
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .serializers import OrderItemSerializer, WidgetSerializer
from .models import Gate, OrderItem, Widget


class OrderItemViewSet(ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer

    @action(detail=False, methods=['post'],
            url_path='check_gate', url_name='check_gate')
    def check_gate(self, request, *args, **kwargs):
        # без валидации здесь))
        time = parse(request.data.get('date'))
        objs = Gate.objects.filter(
            Q(number=request.data.get('gate')) &
            (Q(start__lte=time) & Q(end__gte=time)))
        if objs.count() > 0:
            return Response({'warning': 'Ворота заняты в это время!'},
                            status=status.HTTP_409_CONFLICT)
        return Response({}, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'],
            url_path='change_gate', url_name='change_gate')
    def change_gate(self, request, *args, **kwargs):
        # без валидации и здесь))
        obj = OrderItem.objects.filter(id=request.data.get('order')).get()
        obj.gate.number = request.data.get('gate_number')
        obj.save()
        serializer = self.get_serializer(obj)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED, headers=headers)


    def create(self, request, *args, **kwargs):
        _data = request.data.copy()
        gate = Gate(**{
            'number': request.data.get('gate'),
            'start': parse(request.data.get('start'))
        })
        gate.save()
        _data['gate'] = gate.id
        serializer = self.get_serializer(data=_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


    def update(self, request, *args, **kwargs):
        _data = request.data.copy()
        del _data['gate']
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=_data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        gate = Gate.objects.filter(orderitem__id=request.data.get('id')).get()
        gate.start = parse(request.data.get('start'))
        gate.number = request.data.get('gate_number')
        gate.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)


class WidgetViewSet(ModelViewSet):
    queryset = Widget.objects.all()
    serializer_class = WidgetSerializer