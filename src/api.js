export function isGateBusy (localThis, gate, date) {
  localThis.axios.post(
    'http://127.0.0.1:8000/api/orders/check_gate/',
    {gate: gate, date: new Date(date).toISOString()}
  )
    .then(() => { // все хорошо
      return false
    })
    .catch(e => { // сервер отдает ошибку 409 CONFLICT
      alert(e.response.data.warning)
      return true
    })
}

export function changeOnlyGateApi(localThis, order, gateNumber) {
  localThis.axios.post(
    'http://127.0.0.1:8000/api/orders/change_gate/',
    { order: order, gate_number: gateNumber }
  )
    .then(resp => {
      // закрыть popup
      localThis.$root.$emit('bv::hide::modal', 'gate-' + resp.data.gate)
    })
    .catch(() => {
      alert('Упс, что-то пошло не так...')
    })
}