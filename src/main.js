/* JS */
import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import SimpleVueValidation from 'simple-vue-validator'
import Icon from 'vue-awesome/components/Icon'
import store from './store/index'
/* CSS */
import '@/assets/base.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

/* Vue */
Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(SimpleVueValidation)
Vue.use(BootstrapVue)
Vue.component('fa-icon', Icon)

/* шина */
window.vue_bus = new Vue()

new Vue({
  el: '#app',
  store,
  render: h => h(App)
}).$mount('#app')