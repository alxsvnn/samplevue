import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    orders: null,
    widgets: null
  },
  getters: {
    getOrders: state => state.orders,
    getWidgets: state => state.widgets,
    getWidgetsList: (context) => {
      let _tmpArr = []
      _.forEach(context.widgets, function (o) { _tmpArr.push(o[0]) })
      return _tmpArr
    },
  },
  mutations: {
    addWidgets(state, data) {
      state.widgets = Object.values(_.groupBy(data, 'id'))
    },
    addOrders(state, data) {
      state.orders = Object.values(_.groupBy(data, 'widget'))
    },
    addOrder(state, data) {
      let added = false
      state.orders.forEach(function (widget, index) {
        if (widget[0].widget === data.widget) {
          state.orders[index].push(data)
          added = true
        }
      })
      if (!added) state.orders.push([data])
    },
    setNewGateNumber(state, data) {
      _.forEach(state.orders, function (o) {
        let _search = _.find(o, ['id', data.order]) // найти
        if (_search) _search.gate_number = data.gate_number // изменить
      })

    }
  },
  actions: {
    loadWidgets(context) {
      // можно тут еще какие-нибудь проверки, кэш, localStorage, что вздумается в общем
      Vue.axios('http://127.0.0.1:8000/api/widgets/')
        .then(resp => {
          context.commit('addWidgets', resp.data)
        })
        .catch(e => {
          alert(e)
        })
    },
    loadOrders(context) {
      Vue.axios('http://127.0.0.1:8000/api/orders/')
        .then(resp => {
          context.commit('addOrders', resp.data)
        })
        .catch(e => {
          alert(e)
        })
    },
    addOrder(context, data) {
      Vue.axios.post('http://127.0.0.1:8000/api/orders/', data )
        .then(resp => {
          context.commit('addOrder', resp.data)
          window.vue_bus.$emit('close::modal', 'order-add')
        })
        .catch(e => {
          alert(e)
        })
    },
    updateOrder(context, data) {
      Vue.axios.put('http://127.0.0.1:8000/api/orders/' + data.get('id') + '/', data)
        .then(resp => {
          window.vue_bus.$emit('close::modal', resp.data.id)
        })
        .catch(e => {
          alert(e)
        })

    }
  },
  plugins: [createPersistedState()]
})