export function showCorrectDate(date) {
  let dt = new Date(date)
  let currTZ = new Date().getTimezoneOffset()
  dt.setMinutes(currTZ*2)
  let options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
  }
  return dt.toLocaleString('ru', options)
}